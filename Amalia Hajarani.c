#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <windows.h>
#include <process.h>
#include <time.h>
#include <math.h>

//Variabel yang digunakan
	struct Data3{
		int i;
		char ch;
	}value3();
	
	struct Data5{
		int i;
		char ch;
	}value5();
	
	struct hitungscore{
		char name[20];
		char level;
		float score;
	}score;
	
	typedef enum {false=0, true=1} bool;
	
//Modul yang digunakan
	void gotoxy(int x, int y);
	void judul();
	void loading();
	void layarutama();
	int  timercounter();
	void menyimpanscore();
	void menuutama();
	void instruction();
	void play();
	int  pilihpapanlevel();
	void easy3();
	void easy5();
	void board3(char sym[9]);
	void board5(char sym[25]);


void gotoxy(int x, int y) 
{
	//Modul untuk menempatkan posisi output saat ditampilkan dilayar
	// x: variabel untuk menempatkan posisi di sumbu x
	// y: variabel untuk menempatkan posisi di sumbu y
	COORD coord;
	coord.X = x;
	coord.Y = y;
	
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),coord);
}

void judul()
{
	//modul untuk menampilkan judul permainan pada awal permainan
	gotoxy(19,5); printf(" *******************       *************         ************\n");
	gotoxy(19,6); printf(" *******************       *************        *********\n");
	gotoxy(19,7); printf("        ****                   ****            *******\n");
	gotoxy(19,8); printf("        ****                   ****           *******\n");
	gotoxy(19,9); printf("        ****                   ****          *****\n");
	gotoxy(19,10); printf("        ****                   ****          ******\n");
	gotoxy(19,11); printf("        ****                   ****          *******\n");
	gotoxy(19,12); printf("        ****                   ****           ********\n");
	gotoxy(19,13); printf("        ****               *************       *********\n");
	gotoxy(19,14); printf("        ****               *************         ************\n");
}

void loading()
{
	//modul untuk menampilkan loading  bersamaan dengan judul permainan
	int r, q;
	
	gotoxy(50,16);printf("Loading...");
	gotoxy(45,18);for(r = 1; r<20; r++)
					{
						for(q = 0; q<=130000000; q++);
						printf("%c", 219);
					}
}

void layarutama()
{
	//modul untuk memanggil modul judul dan loading pada saat bersamaan saat permainan dimulai
	judul();
	loading();
}

void menuutama()
{ 
	//modul untuk menampilkan pilihan yang dapat dipilih sesuai keinginan pengguna
	
	int pilih;
	
	system("CLS");
	
	gotoxy(25,5);puts(" _________________________");
	gotoxy(25,6);puts("|        Main Menu        |");
	gotoxy(25,7);puts("|=========================|");
	gotoxy(25,8);puts("| 1) Play                 |");
	gotoxy(25,9);puts("| 2) Instructions         |");
	gotoxy(25,10);puts("| 3) Scores               |");
	gotoxy(25,11);puts("| 4) Exit                 |");
	gotoxy(25,12);puts("|=========================|");
	gotoxy(25,13);puts("|     Type the number     |");
	gotoxy(25,14);puts("|       to choose :       |");
	gotoxy(25,15);puts("|_________________________|");
	
	gotoxy(45,14);scanf("%d", &pilih);
	
	switch(pilih)
	{
		case 1 : play();
				 break;
		case 2 : instruction();
				 break;
//		case 3 :
//				 break;
		case 4 : exit(0);
	}
}
void instruction()
{	//modul untuk menampilkan tata cara dan aturan bermain permainan

	int pilih;

	system("CLS");
	
	gotoxy(25,5);puts("    ///////////////////////////////");
	gotoxy(25,6);puts("   ///////////////////////////////");
	gotoxy(25,7);puts("///                              ///");
	gotoxy(25,8);puts("/// ---------------------------- ///");
	gotoxy(25,9);puts("/// How to Play Tick (Tac Toe) : ///");
	gotoxy(25,10);puts("/// ---------------------------- ///");
	gotoxy(25,11);puts("/// [1] All user defined by 'X'  ///");
	gotoxy(25,12);puts("///     icon                     ///");
	gotoxy(25,13);puts("/// [2] Choose level and board   ///");
	gotoxy(25,14);puts("/// [3] Type a number to place   ///");
	gotoxy(25,15);puts("///     your icon.               ///");
	gotoxy(25,16);puts("/// [4] You have 8 seconds for   ///");
	gotoxy(25,17);puts("///     Easy Level and 5 seconds ///");
	gotoxy(25,18);puts("///     for Hard Level           ///");
	gotoxy(25,19);puts("/// ---------------------------- ///");
	gotoxy(25,20);puts("///  Press '0' for back to menu  ///");
	gotoxy(25,21);puts("///                              ///");
	gotoxy(25,22);puts("   //////////////////////////////");
	gotoxy(25,23);puts("   /////////////////////////////");
	
	gotoxy(40,21);scanf("%d", &pilih);
	
	if(pilih == 0)
		{
			menuutama();
		} 
}

void play()
{
	//modul untuk memanggil modul yang dibutuhkan saat bermain
	pilihpapanlevel();
	
}

int pilihpapanlevel()
{
	int memilih;
	
	system("CLS");
	
	gotoxy(25,5);puts(" _________________________");
	gotoxy(25,6);puts("|Pick Your Board and Level|");
	gotoxy(25,7);puts("|=========================|");
	gotoxy(25,8);puts("| 1) 3 X 3 and Easy       |");
	gotoxy(25,9);puts("| 2) 5 X 5 and Easy       |");
	gotoxy(25,10);puts("| 3) 3 X 3 and Hard       |");
	gotoxy(25,11);puts("| 4) 5 X 5 and Hard       |");
	gotoxy(25,12);puts("| 5) Main Menu            |");
	gotoxy(25,13);puts("|=========================|");
	gotoxy(25,14);puts("|     Type the number     |");
	gotoxy(25,15);puts("|       to choose :       |");
	gotoxy(25,16);puts("|_________________________|");
	
	gotoxy(45,15);scanf("%d", &memilih);
	
	switch(memilih)
	{
		case 1 	: system("CLS");
				  easy3();  
				  score.level = "Easy 3x3";
				  break;
		case 2 	: system("CLS");
				  easy5();
				  score.level = "Easy 5x5";
				  break;
//		case 3 	: system("CLS");
//				  hard3();
//				  break;
//		case 4 	: system("CLS");
//				  hard5();
//		   		  break;
		case 5 	: menuutama();
		          break;
		default : menuutama();
				  break;		         
	}
}

void menyimpanscore()
{
	FILE *fscore = fopen("ScoreTick.txt", "ab");
	fprintf(fscore, "%s			%s			%.0f\n", score.name, score.level, score.score);
	fclose(fscore);
}

void playaagain()
{
	int pilih;
	puts("Do You Want to Play Again?");
	puts("(1) Yes");
	puts("(2) No");
	printf("You choose:  ");
	scanf("%d", &pilih);
	
	switch(pilih)
	{
		case 1: pilihpapanlevel();
				break;
		case 2: menuutama();
				break;					
	}
}

void easy3()
{
	int count 		= 0;
	struct Data3 info;
	char symbol[9] 	= {'1','2','3','4','5','6','7','8','9'};
	board3(symbol);
	clock_t t; 
	
	t = clock(); //timer started
	again:
	
	//ifnya disini buat easy sama hardnya.	
	info 			= value3(symbol, count);
	symbol[info.i] 	= info.ch;
	
	system ("cls");
	
	board3(symbol);
		if (cekikon3(symbol, info.ch, count)==1 && count % 2 == 0)
		{ 
			t = clock() - t; //timer ended
			printf ("Game Is Done\n");
      		double time_taken = ((double)t)/CLOCKS_PER_SEC;
      		//asigning score
      		score.score = (100 - time_taken)*10;
      		menyimpanscore();
      		
			playaagain();
		}
		
		if (cekikon3(symbol, info.ch, count)==1 && count % 2 != 0)
		{ 
			t = clock() - t; //timer ended
			printf ("Game Is Done\n");
      		double time_taken = ((double)t)/CLOCKS_PER_SEC;
      		//asigning score
      		score.score = 0;
      		menyimpanscore();
      		
			playaagain();
		}
		
		else if (cekikon3(symbol, info.ch, count)==0)
		{ 
			t = clock() - t; //timer ended
			printf ("Game Is Draw\n");
   			double time_taken = ((double)t)/CLOCKS_PER_SEC;
   			//asigning score
   			score.score = (100 - time_taken)*0;
   			menyimpanscore();
   			
			playaagain();
		}
		else
		{
		count++;
		goto again;
		}
}

void easy5()
{
	int count = 0;
	struct Data5 info;
	char symbol[50] = {'a','b','c','d','e','f','g','h','i','j',
				       'k','l','m','n','o','p','q','r','s','t',
				       'u','v','w','x','y'};
	board5(symbol);
	clock_t t;
	
	t = clock(); //timer started
	again:
		
	info 			= value5(symbol, count);
	symbol[info.i] 	= info.ch;
	
	system("cls");
	
	board5(symbol);
		if(cekikon5(symbol, info.ch, count)==1 && count % 2 == 0)
		{ 
			t = clock() - t; //timer ended
			printf ("Game Is Done\n");
      		double time_taken = ((double)t)/CLOCKS_PER_SEC;
      		//asigning score
      		score.score = (200 - time_taken)*10;
      		menyimpanscore();
      		
			playaagain();
		}
		else if(cekikon5(symbol, info.ch, count)==1 && count % 2 != 0)
		{ 
			t = clock() - t; //timer ended
			printf ("Game Is Done\n");
      		double time_taken = ((double)t)/CLOCKS_PER_SEC;
      		//asigning score
      		score.score = 0;
      		menyimpanscore();
      		
			playaagain();
		}
		else if(cekikon5(symbol, info.ch, count)==0)
		{ 
			t = clock() - t; //timer ended
			printf ("Game Is Draw\n");
      		double time_taken = ((double)t)  /CLOCKS_PER_SEC;
      		//asigning score
      		score.score = 0;
      		menyimpanscore();
      		
			playaagain();
		}
		else
		{
			count++;
			goto again;
		}
}

int cekikon3(char sym[9], char ch[1], int count)
{
	int i;
	//pengecekan secara horizontal
	for (i = 0; i < 8; i=i+3)
	{
		if (sym[i] == sym [i+1] && sym[i+1] == sym[i+2])
		{	
			printf("the Winner is: %c", ch); return 1;
		}
	}
	
	//pengecekan secara vertikal
	for (i = 0; i < 3; i++)
	{
		if (sym[i] == sym [i+3] && sym[i+3] == sym[i+6])
		{
			printf("the Winner is: %c", ch); return 1;
		}
	}
	
	//pengecekan secara diagonal
	if (sym[0] == sym [4] && sym[4] == sym[8])
	{
		printf("the Winner is: %c", ch); return 1;
	}
	else if (sym[2] == sym [4] && sym[4] == sym[6])
	{
		printf("the Winner is: %c", ch); return 1;
	}
	else if (count == 8)
	{
		printf("We Draw!");
		return 0;
	}
	
	else return -1;
}

int cekikon5(char sym[25], char ch[1], int count)
{
	int i;
	//pengecekan secara horizontal
	for (i = 0; i < 24; i=i+5)
	{
		if(sym[i] == sym [i+1] && sym[i+1] == sym[i+2] && sym[i+2] == sym [i+3] ||
		   sym[i+1] == sym [i+2] && sym[i+2] == sym[i+3] && sym[i+3] == sym [i+4])
		{	
			printf("the Winner is: %c", ch); return 1;
		}
	}
	
	//pengecekan secara vertikal
	for (i = 0; i < 5; i++)
	{
		if(sym[i] == sym [i+5] && sym[i+5] == sym[i+10] && sym[i+10] == sym [i+15] ||
		   sym[i+5] == sym [i+10] && sym[i+10] == sym[i+15] && sym[i+5] == sym [i+20])
		{
			printf("the Winner is: %c", ch); return 1;
		}
	}
	
	//pengecekan secara diagonal
	for (i = 0; i < 2; i++)
	{
		if(sym[i] == sym [i+6] && sym[i+6] == sym[i+12] && sym[i+12] == sym [i+18] ||
		   sym[i+6] == sym [i+12] && sym[i+12] == sym[i+18] && sym[i+18] == sym [i+24])
		{
			printf("the Winner is: %c", ch); return 1;
		}
	}
	
	for (i = 3; i < 5; i++)
	{
		if(sym[i] == sym [i+4] && sym[i+4] == sym[i+8] && sym[i+8] == sym [i+12] ||
		   sym[i+4] == sym [i+8] && sym[i+8] == sym[i+12] && sym[i+12] == sym [i+16])
		{
			printf("the Winner is: %c", ch); return 1;
		}
	}
	
	if(sym[5] == sym [11] && sym[11] == sym[17] && sym[17] == sym [23])
	{
		printf("the Winner is: %c", ch); return 1;
	}
	
	else if(sym[9] == sym [13] && sym[13] == sym[17] && sym[17] == sym [21])
	{
		printf("the Winner is: %c", ch); return 1;
	}
	
	//pengecekan jika papan terisi penuh
	else if (count == 25)
	{
		printf("We Draw!");
		return 0;
	}
	
	else return -1;
}

struct Data3 value3(char sym[9], int count) //modul puikon
{
	char value[1];
	char nilai;
	int i;
	struct Data3 info;
	
	inputAgain:
	
	if (count%2 == 0)
	{
		printf("\n Choosse your choice, Hooman: ");
		scanf("%s", &value);
		for (i=0; i<9; i++)
		{
			if (*value == sym[i])
			{
				info.i = i;
				info.ch = 'X';
				break;
			}else
			{
				info.i = -1;
				info.ch = " ";
			}
		}
		if (info.i == -1)
		{
			printf("\nInput is not valid");
			goto inputAgain;
		}
		
		return info;
	}else
	{   //note: ini bisa pake if buat hard sama easy
		nilai = vereasy3();
		for (i=0; i<9; i++)
		{
			if (nilai == sym[i])
			{
				info.i = i;
				info.ch = 'O';
				break;
			}else
			{
				info.i = -1;
				info.ch = " ";
			}
		}
		
		if (info.i == -1)
		{
			goto inputAgain;
		}
		
		return info;
	}	
}

struct Data5 value5(char sym[25], int count)
{
	char value[2];
	char nilai;
	int i;
	struct Data5 info;
	
	inputAgain:
	
	if (count%2 == 0)
	{
		printf("\n Choosse your choice, Hooman: ");
		scanf("%s", &value);
		for(i=0; i<25; i++)
		{
			if(*value == sym[i])
			{
				info.i = i;
				info.ch = 'X';
				break;
			}else
			{
				info.i = -1;
				info.ch = " ";
			}
		}
		if(info.i == -1)
		{
			printf("\nInput is not valid");
			goto inputAgain;
		}
		
		return info;
	}else
	{
		nilai = vereasy5();
		for(i=0; i<25; i++)
		{
			if(nilai == sym[i])
			{
				info.i = i;
				info.ch = 'O';
				break;
			}else
			{
				info.i = -1;
				info.ch = " ";
			}
		}
		if(info.i == -1)
		{
			goto inputAgain;
		}
		return info;
	}	
}

int vereasy3()
{
	srand (time(0));
	int random_number;
	char value; 
	
	random_number 	= 1 + rand() % 8;
	value 			= random_number + '0';
	
	return value ;
}

int vereasy5() //modul untuk pergerakan komputer
{
    srand(time(0));
	char randomhuruf;
	char value;
	randomhuruf = "abcdefghijklmpqrstuvwxy" [rand() % 24];
	value = randomhuruf;
	
	return value ;
}

void board3(char sym[9]){
	gotoxy(25,5);printf("Let's Play!");
	printf("\n\t\t\t _________________________");
	printf("\n\t\t\t|        |        |       |");
	printf("\n\t\t\t|   %c    |    %c   |   %c   |", sym[0], sym[1], sym[2]);
	printf("\n\t\t\t|________|________|_______|");
	printf("\n\t\t\t|        |        |       |");
	printf("\n\t\t\t|   %c    |    %c   |   %c   |", sym[3], sym[4], sym[5]);
	printf("\n\t\t\t|________|________|_______|");
	printf("\n\t\t\t|        |        |       |");
	printf("\n\t\t\t|   %c    |    %c   |   %c   |", sym[6], sym[7], sym[8]);
	printf("\n\t\t\t|________|________|_______|");	
}

void board5(char sym[25]){
	printf("Tic Tac Toe");
	printf("\n\t\t\t _________________________________________");
	printf("\n\t\t\t|        |        |       |       |       |");
	printf("\n\t\t\t|   %c    |   %c    |   %c   |   %c   |   %c   |", sym[0], sym[1], sym[2], sym[3], sym[4]);
	printf("\n\t\t\t|________|________|_______|_______|_______|");
	printf("\n\t\t\t|        |        |       |       |       |");
	printf("\n\t\t\t|   %c    |   %c    |   %c   |   %c   |   %c   |", sym[5], sym[6], sym[7], sym[8], sym[9]);
	printf("\n\t\t\t|________|________|_______|_______|_______|");
	printf("\n\t\t\t|        |        |       |       |       |");
	printf("\n\t\t\t|   %c    |   %c    |   %c   |   %c   |   %c   |", sym[10], sym[11], sym[12], sym[13], sym[14]);
	printf("\n\t\t\t|________|________|_______|_______|_______|");
	printf("\n\t\t\t|        |        |       |       |       |");
	printf("\n\t\t\t|   %c    |   %c    |   %c   |   %c   |   %c   |", sym[15], sym[16], sym[17], sym[18], sym[19]);
	printf("\n\t\t\t|________|________|_______|_______|_______|");
	printf("\n\t\t\t|        |        |       |       |       |");
	printf("\n\t\t\t|   %c    |   %c    |   %c   |   %c   |   %c   |", sym[20], sym[21], sym[22], sym[23], sym[24]);
	printf("\n\t\t\t|________|________|_______|_______|_______|");
}



int main()
{
//	layarutama();
//	system("CLS");
	printf("Please Enter Your Name: \n");
	gets(score.name);
	menuutama();
	return 0;
}
